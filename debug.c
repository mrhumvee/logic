#include "debug.h"

int warningLimit=-1;
void debugPrint(int limit,const char * fmt,...)
	{
		//printf("\n");
		if (warningLimit== -1) warningLimit = limit;	
		if (warningLimit==99) fprintf(stderr,"limit is %d, warningLimit is %d\n",limit,warningLimit);
		if (limit <=warningLimit){
			va_list args;
			va_start(args,fmt);
			vfprintf(stderr,fmt,args);
			va_end(args);
		}
		if (warningLimit == 99) fprintf(stderr,"end DebugPrint\n");
	}
int instack(char * const st, int stackCounter,const char * filename, int line){
	TRACE((1,"%s:%d stack : ",filename,line));
	int i;
	for (i=0; i<=stackCounter; i++){
		if (is_alphabet(*(st+i))) 
			TRACE((1,"%c ",*(st+i)));
		else TRACE((1,"%d ",*(st+i)));
	}
	TRACE((1,"\n"));
	return 0;
}

