
#include "logic.h"
#include "debug.h"

char trangThai(int count, char position){ 
	//trang thai cua 1 bien dua tren count
	position--;
	char result=1;
	if ((count & (1 << position )) ==0) 
		result= 0;
	TRACE((1,"%s:%d trang thai tai vi tri %d cua so %d : %d\n",__FILE__ , __LINE__,position, count, result));
	return result;
}

char tinhtoan2(char pheptoan,char so1, char so2, int count){
	//xet luon truong hop so1, so2 la 1 ket qua 1 bit 0 hoac 1
	char num1,num2,result;
	if (so1 <0) num1=trangThai(count,abs(so1));
	else num1=so1;
	if (so2 <0) num2=trangThai(count,abs(so2));
	else num2=so2;

	if (pheptoan == 15) 
		result = ((num1 & num2) & 1);
	if (pheptoan == 14)
		result = ((num1 | num2) & 1);
	if (pheptoan == 13)
		{
			if ((num1==1) && (num2==0))
				result = 0;
			else result =1;
		}
	if (pheptoan == 12)
		result = ((~(num1 ^ num2)) & 1);

	TRACE((1,"%s:%d Voi count=%d, %d giua %d state %d va %d state %d, result tinh toan 2 :%d\n",__FILE__ , __LINE__,count,pheptoan,so1,num1,so2,num2,result));
	return result;
}

char tinhtoan1(char so1,int count){
	char num1,result;
	if (so1 < 0) num1=trangThai(count,abs(so1));
	else num1=so1;
	result = ((~num1) & 1);
	TRACE((1,"%s:%d num1 is %d, ~num1 is %d, ((~num1) &1 is %d\n",__FILE__ , __LINE__,num1,~num1,((~num1)&1)));
	TRACE((1,"%s:%d Voi count=%d, not %d state = %d, result tinh toan 1 :%d\n",__FILE__ , __LINE__,count,so1,num1,result));
	return result;
}

char demBienLogic(const char * st, char ** tieuDeBienLogic){
	char letter[26];
	int i;
	char dem=0;
	char* ptrBienLogic = NULL;
	ptrBienLogic=(char*) calloc(26,sizeof(char));
	if (ptrBienLogic == NULL)
		{
			printf("Error Allocating Memory in demBienLogic.\n");
			free(ptrBienLogic);
			return 100;
		}
	//cleanChar(ptrBienLogic,26);
	for (i=0;i<=25;i++)
		letter[i]=0;
	for (i=(strlen(st)-1); i>=0; i--)
		{
			if ((letter[st[i]-'a']==0) && (is_alphabet(st[i])))
				{
					TRACE((1,"%s:%d Dem bien logic %c : %d \n ",__FILE__ , __LINE__,st[i],dem));
					*(ptrBienLogic+dem)=st[i];
					dem++;
					letter[st[i]-'a']=1;
				}
		}
	(*tieuDeBienLogic) = ptrBienLogic;
	return (dem-1);
}

char*  danhDauKyTu(const char * str){ //danh dau ky tu bang so am
					//luu y van con chu v dung cho OR
	int i=0;
	signed char count = -1;
	char * ptr = (char *) calloc(inputlength,sizeof(char));
	if (ptr ==NULL)
			{
				printf("Error Allocating Memory in danhDauKyTu.\n");
				free(ptr);
				return NULL;
			}
	char letter[26];
	for (i=0;i<=25;i++)
		letter[i]=0;
	for (i=0; i<=(strlen(str)); i++){
		*(ptr+i) = *(str+i);
	}

	for (i=(strlen(str)-1); i>=0; i--){
		if (is_alphabet(str[i]))
			{
				TRACE((1,"%s:%d meet %c \n",__FILE__ , __LINE__,str[i]));
				if (letter[str[i]-'a']==0)
					{
						TRACE((1,"%s:%d first time met, assigned %d \n",__FILE__ , __LINE__,count));
						*(ptr+i)=count;
						letter[str[i]-'a']=count;
						count--;
					}
				else 	{
						*(ptr+i)=letter[str[i]-'a'];
						TRACE((1,"%s:%d already met, assigned %d \n",__FILE__ , __LINE__,letter[str[i]-'a']));
					}
			}
	} 

	TRACE((1,"Thuc hien Danh Dau Ky Tu \n"));
	do {if (DEBUG) {
		for (i=0;i<=(strlen(str)-1);i++){
			if (((*(i+ptr)>=10) && (*(i+ptr)<=16)) || (*(i+ptr)<0))
				TRACE((1,"%d ",*(i+ptr)));
			else 
				TRACE((1,"%c ", *(i+ptr)));
		}
		TRACE((1,"\n"));
	}} while (0);
	return ptr;
}

int findIndent(char * const indent, int count, char * ngoac){
	char i=0;
	for (i=0;i<=count;i++)
		{
			if (!i)
				indent[i]=(*(ngoac+(i+1)*2-1)/2);
			else indent[i]=( *(ngoac+(i+1)*2-1)/2 + *(ngoac+((i-1+1)*2-1))/2);

			do { if (DEBUG) {
				TRACE((1,"i is %d\n",i));
				TRACE((1,"length is : %d \n",*(ngoac+(i+1)*2-1)/2));
				TRACE((1,"length previous is : %d \n",*(ngoac+((i-1+1)*2-1))/2));
				TRACE((1,"indent[%d] : %d \n",i,indent[i]));
			}} while (0);
		}	
	return 0;
}

int xuatTrangThai(char soLuongBien,int count){
	int i;
	for (i=soLuongBien;i>=0;i--){
		if ((count & (1 << i)) !=0 )
			printf("%d ",1);
		else printf("%d ",0);
	}
	return 0;			
}

int xuatTieuDe(char* tieuDeBien, char soLuongBien, char* ngoac, int ngoacCount, char* const strOrigin){
//xuat bien tu ben phai cung ra truoc
	TRACE((1,"%s:%d tieuDeBien : %s\n",__FILE__ , __LINE__,tieuDeBien));
	TRACE((1,"%s:%d soLuongBien: %d (dem tu 0)\n",__FILE__ , __LINE__,soLuongBien));
	TRACE((1,"%s:%d strOrigin: %s\n",__FILE__ , __LINE__,strOrigin));	
	TRACE((1,"%s:%d ngoacCount: %d\n",__FILE__ , __LINE__,ngoacCount));	
	char chari;
	for (chari=soLuongBien;chari>=0;chari--)
		{
			printf("%c ",*(tieuDeBien+chari));
		}
	int i=0;
	while (i<=ngoacCount) 
		{
			char * ptr = (char*) calloc(inputlength,sizeof(char));
			if (ptr == NULL) {
					printf("Error Allocating Memory in xuatTieuDe.\n");
					free(ptr);
					return 2;
			}
			strncpy(ptr,strOrigin + *(ngoac+i),*(ngoac+i+1));
			printf("%s ", ptr);
			free(ptr);
			i=i+2;	
		}
	printf("\n");
	return 0;
}

char * tachDonThuc(const char * str,int * capNgoacCountResult){
	#define limitCapNgoac 200
	TRACE((1,"Danh dau ky tu, 1 cap = start & length\n"));

	char moNgoac[100];
	//int * capNgoac= (int*) calloc(limitCapNgoac,1);
	char * capNgoac= (char*) calloc(inputlength,sizeof(char));
	if (capNgoac == NULL) 
		{
			printf("error allocating memory in tachDonThuc.\n");
			free(capNgoac);
			return NULL;
		}
	char moNgoacCount=0;
	char capNgoacCount=0;
	char ngoacMo;
	char ngoacDong;
	char i;
	/*for (i=0;i<=(limitCapNgoac/4);i++){
		moNgoac[i]=0;
		*(capNgoac+4*i)=0;
	}*/	
	for (i=0;i<=(strlen(str)-1);i++){
		TRACE((1,"%s:%d meet %c\n",__FILE__ , __LINE__,str[i]));
		if (str[i]=='(')
			{
				moNgoac[moNgoacCount++]=i;
			}
		if (str[i]==')')
			{
				ngoacMo=moNgoac[--moNgoacCount];
				ngoacDong=i;
				*(capNgoac+capNgoacCount++)=ngoacMo;
				*(capNgoac+capNgoacCount++)=ngoacDong-ngoacMo+1;
			}
	}
	if ((ngoacDong-ngoacMo+1)<strlen(str))
		{
			*(capNgoac+capNgoacCount++)=0;
			*(capNgoac+capNgoacCount++)=strlen(str);
		}	
		TRACE((1,"Tach Don Thuc : \n"));
		TRACE((1," %s \n",str));
		do {if (DEBUG){
		for (i=0;i<=capNgoacCount;i++){
			TRACE((1,"%d ",*(capNgoac+i)));
		}
		TRACE((1,"\n"));
		}} while (0);

	*capNgoacCountResult=--capNgoacCount;
	return capNgoac;
}

int tinhGiaTriVaXuat(const char * strBaLan,char * const strOrigin){ // phan tich chuoi ba lan de tinh gia tri cua 1 bien dua tren trangThai
						
		char stack[100];
		int stackCounter=0;
		int i=0;
		char ketqua=0;
		int count=0;

		
		int capNgoacCount=0;
		char * ngoac = tachDonThuc(strOrigin,&capNgoacCount);
		if (ngoac == NULL)
				return 2;

		int indentCount=0;
		indentCount=(capNgoacCount+1)/2-1;
		char indent[100];
		int xuatCount=0;

		char tempValue=5;
		char* tieuDeBienLogic = &tempValue;
		char soLuongBien=demBienLogic(strBaLan,&tieuDeBienLogic);

		if (soLuongBien == 100) return 2;

		int soLuongTruongHop= (int) pow((double)2,(double) (soLuongBien+1))-1;
		TRACE((1,"%s:%d soLuongTruongHop : %f\n",__FILE__ , __LINE__,pow((double)2,(double)(soLuongBien+1))));
			
		if (xuatTieuDe(tieuDeBienLogic,soLuongBien,ngoac,capNgoacCount,strOrigin) ==2) 
			return 2;
		free(tieuDeBienLogic);
		
		findIndent(indent,indentCount,ngoac);

		char* strMarked = danhDauKyTu(strBaLan); //free later
		if (strMarked == NULL)
			{
				free(strMarked);
				return 2;
			}

	for (count =0; count<=soLuongTruongHop; count++)
	{	
		xuatTrangThai(soLuongBien,count);

		for (i=0;i<=(strlen(strBaLan)-1);i++){
			TRACE((1,"%s:%d meet %d \n",__FILE__ , __LINE__, strMarked[i]));

			if (strMarked[i]<0) 
				{
					stack[stackCounter]=strMarked[i];
					instack(stack,stackCounter, __FILE__ , __LINE__ );
					stackCounter++;
					TRACE((1,"%s:%d stackCounter: %d\n",__FILE__ , __LINE__,stackCounter));
				}
			else if (strMarked[i] != 16)
				{
					ketqua = tinhtoan2(strMarked[i],stack[stackCounter-2],stack[stackCounter-1],count);
					printf("%*d ",indent[xuatCount],ketqua);
					xuatCount++;
					stack[stackCounter-2]=ketqua; //ket qua 1 bit 1 0
					instack(stack,stackCounter, __FILE__ , __LINE__ );

					stackCounter--;
					TRACE((1,"%s:%d stackCounter: %d\n",__FILE__ , __LINE__,stackCounter));
				}
			else {
				ketqua = tinhtoan1(stack[stackCounter-1],count);
				printf("%*d ",indent[xuatCount],ketqua);
				xuatCount++;

				stack[stackCounter-1]=ketqua; //ket qua 1 bit 1 hay 0
				TRACE((1,"%s:%d stackCounter: %d\n",__FILE__ , __LINE__,stackCounter));
				TRACE((1,"%s:%d after tinhtoan1, stack[stackCounter-1]: %d\n",__FILE__ , __LINE__,stack[stackCounter-1]));

				}
		}
		printf("\n");
		stackCounter=0;
		xuatCount=0;
	}
	free(strMarked);
	return 0;
	}
