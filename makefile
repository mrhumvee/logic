logic : logic.o giatri.o debug.o debug.h logic.h
	gcc -g logic.o giatri.o debug.o -o logic -lm

logicexpsrc.o : logic.c logic.h debug.h
	gcc -c -g logic.c -o logic.o

giatri.o : giatri.c logic.h debug.h
	gcc -c  -g giatri.c -o giatri.o

debug.o : debug.c debug.h
	gcc -c -g debug.c -o debug.o

logic.o : logic.c logic.h debug.h
	gcc -c -g logic.c -o logic.o
